﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Titles.ViewModel;
using Titles.Models;

namespace Titles.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(SearchTitleViewModel viewModel, string titleId)
        {
            ViewBag.Message = "Search and look up Title information";
            if (!string.IsNullOrEmpty(viewModel.SearchTitleName))
            {
                using (TitlesEntities dbContext = new TitlesEntities())
                {
                    var titles = dbContext.Titles.Where(item => item.TitleName == viewModel.SearchTitleName);
                    List<Title> titleList = titles.ToList();
                    var titleModelList = titleList.Select(item => new TitleViewModel()
                                                                       {
                                                                           TitleId = item.TitleId,
                                                                           TitleName = item.TitleName,
                                                                           TitleNameSortable = item.TitleNameSortable,
                                                                           TitleTypeId = item.TitleTypeId,
                                                                           ReleaseYear = item.ReleaseYear,
                                                                           ProcessedDateTime = item.ProcessedDateTimeUTC.Value.ToString("MM/dd/yyyy hh:mm:ss tt")
                                                                       }).ToList();
                    viewModel.SearchResults = titleModelList;
                    Session["Titles"] = titleModelList;
                    Session["SearchTitle"] = viewModel.SearchTitleName;
                }
            }
            if (!string.IsNullOrEmpty(titleId))
            {
                using (TitlesEntities dbContext = new TitlesEntities())
                {
                    int id = Convert.ToInt32(titleId);
                    var title = dbContext.Titles.First(item => item.TitleId == id);
                    viewModel.SelectedTitle = new TitleViewModel();
                    viewModel.SelectedTitle.TitleName = title.TitleName;
                    viewModel.SelectedTitle.ReleaseYear = title.ReleaseYear;
                    viewModel.SearchTitleName = Session["SearchTitle"].ToString();
                    viewModel.SearchResults = (List<TitleViewModel>)Session["Titles"];
                }
            }
            return View(viewModel);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        //public ActionResult SelectTitle(string titleId)
        //{
        //    if (!string.IsNullOrEmpty(titleId))
        //    {
        //        using (TitlesEntities dbContext = new TitlesEntities())
        //        {
        //            //var title = dbContext.Titles.Where(item => item.TitleId = titleId);

        //        }
        //    }
        //}
    }
}
