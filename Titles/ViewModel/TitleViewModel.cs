﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Titles.ViewModel
{
    public class SearchTitleViewModel
    {
        public string SearchTitleName { get; set; }

        public List<TitleViewModel> SearchResults { get; set; }

        public TitleViewModel SelectedTitle { get; set; }
    }

    public class TitleViewModel
    {
        public int? TitleId { get; set; }
        public string TitleName {get; set;}
        public string TitleNameSortable { get; set; }
        public int? TitleTypeId {get; set;}
        public int? ReleaseYear { get; set; }
        public string ProcessedDateTime { get; set; }
    }
}
